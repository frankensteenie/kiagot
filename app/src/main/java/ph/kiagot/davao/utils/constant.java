package ph.kiagot.davao.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;

import ph.kiagot.davao.R;

/**
 * Created by on 10/17/2016.
 */

public class constant {

    static Context context;
    private static Activity activity;
    private static Dialog dialog;
    private static String url = "http://192.168.254.102:8080/kiagot/";
    private static RequestQueue requestQueue;
    private static Cache cache;
    private static Network network;

    public constant(Activity activity){
        constant.activity = activity;
    }

    private static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        constant.context = context;
    }

    public static Typeface font(Context context){
        getContext();
        Typeface fontt = Typeface.createFromAsset(context.getAssets(), "font.ttf");
        return fontt;
    }
    //tree drawable from drawable
    /*
    * @parameters
    * context, activity, ContextCompat drawable @drawable, id from xml, tree type, x-axis positon, y-axis position
    * */
    public static void tree(final Context context, Activity activity, ImageView tree, int id, int treetype, float x, float y){
        constant.activity = activity;
        getContext();
//        ViewGroup placeHolder;
//        int placeholder = R.id.placeholder;
//        placeHolder = (ViewGroup)activity.findViewById(placeholder);
        tree = (ImageView) constant.activity.findViewById(id);
        tree.setBackground(ContextCompat.getDrawable(context, treetype));
        tree.setX(x);
        tree.setY(y);
//        placeHolder.addView(tree);
    }


    //for building ImageButton on MainActivity
    /*
    * @parameters
    * context, activity, R.id.sample, imagebutton, ContextCompant drawable @drawable folder, x-axis coordinate, y-axis coordinate
    * parse from database information, parse from database information, parse from database information, parse from databse information
    * */
    public static void bldg(final Context context, Activity activity, int id, ImageButton imageButton, int bldgType, float x, float y,
                            final String user, final String information, final String information2, final String infroamtion3){


    }

    /*
    * for houses
    * @parameters
    * context, activity, R.id.sample, imagebutton, ContextCompant drawable @drawable folder, x-axis coordinate, y-axis coordinate
    * parse from database information, parse from database information, parse from database information, parse from databse information
    * */
    public static void house(final Context context, Activity activity, int id, ImageButton imageButton, int housetype, float x, float y,
                             final String user, final String information, final String information2, final String information3){
        final TextView[] tx = new TextView[3];
        final Button[] btn = new Button[3];
        constant.activity = activity;
        getContext();
        //network and cache preparation for parsing from php on mainthread
        cache = new DiskBasedCache(context.getApplicationContext().getCacheDir(), 2048*2048); // 2mb
        network = new BasicNetwork(new HurlStack());
        requestQueue = new RequestQueue(cache, network);
        requestQueue.start();
        //imagebutton for houses positioning ang calling from drawables and listener when it's pressed.
        imageButton = (ImageButton) constant.activity.findViewById(id);
        imageButton.setBackground(ContextCompat.getDrawable(context, housetype));
        imageButton.setX(x);
        imageButton.setY(y);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //android version checker
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    dialog = new Dialog(context, android.R.style.Theme_Holo_Light_Dialog);
                }else{
                    dialog = new Dialog(context);
                }

                dialog.setTitle(user);
                dialog.setContentView(R.layout.dialog);

                tx[0] = (TextView)dialog.findViewById(R.id.textView1);
                tx[0].setText(information);
                tx[0].setTypeface(font(context));
                tx[0].setTextSize(25);

                tx[1] = (TextView)dialog.findViewById(R.id.textView2);
                tx[1].setText(information2);
                tx[1].setTypeface(font(context));
                tx[1].setTextSize(25);

                tx[2] = (TextView)dialog.findViewById(R.id.textView3);
                tx[2].setText(information3);
                tx[2].setTypeface(font(context));
                tx[2].setTextSize(25);

                btn[0] = (Button)dialog.findViewById(R.id.button1);
                btn[0].setText("Get Direction");
                btn[0].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                btn[1] = (Button)dialog.findViewById(R.id.button2);
                btn[1].setText("Cancel");
                btn[1].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

                btn[2] = (Button)dialog.findViewById(R.id.edit);
                btn[2].setText("Edit");
                btn[2].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }
        });
    }
}
