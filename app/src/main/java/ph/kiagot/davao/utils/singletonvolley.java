package ph.kiagot.davao.utils;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by ph.kiagot.davao.utils on 10/25/2016.
 */

public class singletonvolley {

    private static singletonvolley mInstance;
    private RequestQueue requestQueue;
    private static Context mCtx;

    private singletonvolley(Context context){
        mCtx = context;
        requestQueue = getRequestQue();
    }

    public RequestQueue getRequestQue(){
        if(requestQueue == null){
            requestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return requestQueue;
    }

    public static synchronized singletonvolley getmInstance(Context context){
        if(mInstance == null){
            mInstance = new singletonvolley(context);
        }
        return mInstance;
    }

    public<T> void addToRequestQue(Request<T> request){
        requestQueue.add(request);
    }
}