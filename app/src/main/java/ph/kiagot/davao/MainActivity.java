package ph.kiagot.davao;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Display;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;

import ph.kiagot.davao.utils.constant;

public class MainActivity extends Activity {

    ImageButton house1, house3, zoomin, zoomout, house;
    ImageView tree, background;
    RelativeLayout rl;
    LinearLayout ll;
    SearchView searchview;
    String get_user_url = "localhost/kiagot";
    Animation animationZoomin, animationZoomout;

    /**
     * @reference 2
     */
    float[] x = {220.0f, 320.0f, 200.0f, 200.0f, 150.0f, 180.0f, 130.0f, 150.0f, 250.0f, 150.0f};
    float[] y = {150.0f, 200.0f, 200.0f, 120.0f, 200.0f, 250.0f, 250.0f, 350.0f, 250.0f, 250.0f};

    float[] x2 = {400.0f, 150.0f, 250.0f, 400.0f};
    float[] y2 = {1250.0f, 1150.0f, 950.0f, 850.0f};

    /**
     * @reference 1
     */
    float[] xHouse = {110.0f, 180.0f, 200.0f, 250.0f};
    float[] yHouse = {200.0f, 300.0f, 160.0f, 280.0f};

    /**
     * static way of adding the id's
     * should be dynamic waiting for the some more information
     */
    int[] id = {R.id.tree1, R.id.tree2, R.id.tree3, R.id.tree4, R.id.tree5, R.id.tree6, R.id.tree7, R.id.tree8, R.id.tree9, R.id.tree10};
    int[] id2 = {R.id.tree11, R.id.tree12, R.id.tree13, R.id.tree14, R.id.tree15, R.id.tree16, R.id.tree17, R.id.tree18, R.id.tree19, R.id.tree20};
    int[] idHouse = {R.id.house1, R.id.house2, R.id.house3, R.id.house4};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        background = (ImageView)findViewById(R.id.background);
        background.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.kiagotmap));

        //listener if zoom in is pressed or zoom out
        rl = (RelativeLayout)findViewById(R.id.activity_main);
//        ll = (LinearLayout)findViewById(R.id.activity_main);

        //searchview listener with no backend as of now. Still waiting for the information
        searchview = (SearchView)findViewById(R.id.searchview);
        searchview.setIconifiedByDefault(false);
        searchview.setQueryHint("Search Here");
        searchview.setX(10.0f);
        searchview.setBackgroundColor(Color.WHITE);

        //zoomin button and position
        zoomin = (ImageButton) findViewById(R.id.zoomin);
        zoomin.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.zoomin));
        zoomin.setX(400.0f);
        zoomin.setY(580.0f);

        //zoomout button and position
        zoomout = (ImageButton) findViewById(R.id.zoomout);
        zoomout.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.zoomout));
        zoomout.setX(400.0f);
        zoomout.setY(640.0f);

        //@link rl (RelativeLayout when zoom in is pressed it will animate, same for zoomout
        animationZoomin = AnimationUtils.loadAnimation(MainActivity.this, R.anim.zoomin);
        animationZoomout = AnimationUtils.loadAnimation(MainActivity.this, R.anim.zoomout);

//        zoomin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                background.startAnimation(animationZoomin);
//
//            }
//        });

//        zoomout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                background.startAnimation(animationZoomout);
//                Log.d("Touched", "Negative Button");
//            }
//        });

        constant instance = new constant(this);

        //start algo for multi resolution support getting the screen resolution to validate for the positioning of the images.
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        /*
        * @parameters
        * Context, Activity, ID, ImageView, srcFile, x axis value, y axis value, String parse, String parse, String parse.
        * the x and y values are array declared as xHouse, yHouse @reference 1
        * */
        for(int i = 0; i < idHouse.length; i++){
            constant.house(MainActivity.this, this, idHouse[i], house1, R.drawable.housea24, xHouse[i], yHouse[i], "What is Lorem Ipsum?", "What is Lorem Ipsum?",
                    "What is Lorem Ipsum?", "What is Lorem Ipsum?");
        }

        /*
        * context, activity, ImageView, ID, sourceFile, x axis value, y axis value
        * the x and y values are array declared as x, y @reference 2
        * */
        for(int i = 0; i < id.length; i++){
            constant.tree(MainActivity.this, this, tree, id[i], R.drawable.treea24, x[i], y[i]);
        }

        /*
        * context, activity, ImageView, ID, sourceFile, x axis value, y axis value
        * the x and y values are array declared as x2, y2 @reference 2
        * */

        for(int i = 0; i < 4; i++){
            constant.tree(MainActivity.this, this, tree, id2[i], R.drawable.treeb24, x2[i], y2[i]);
        }
    }
}