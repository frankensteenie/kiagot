package ph.kiagot.davao;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

/**
 * Created by ph.kiagot.davao on 10/24/2016.
 */

public class loading extends FragmentActivity {

    private int time = 5000; // 5 seconds
    ImageView spl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        spl = (ImageView)findViewById(R.id.splash);
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(spl);
        Glide.with(loading.this).load(R.raw.splash).into(imageViewTarget);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
            }
        }, time);
    }
}
